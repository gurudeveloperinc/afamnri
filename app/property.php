<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class property extends Model
{
    protected $primaryKey = 'pid';
    public $table = 'properties';

	public function Agent() {
		return $this->belongsTo('App\User','uid');
    }

	public function Images() {
		return $this->hasMany(image::class,'pid');
    }
}
