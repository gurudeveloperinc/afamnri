<?php

namespace App\Http\Controllers;

use App\image;
use App\property;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use League\Flysystem\Adapter\Local;
use League\Flysystem\Exception;
use League\Flysystem\File;
use League\Flysystem\Filesystem;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
    	$properties = property::all()->sortByDesc('created_at');
    	$staff =  User::all();

        return view('home',[
        	'properties' => $properties,
	        'staff' => $staff
        ]);
    }

	public function dashboardProperties() {

		$propertiesByYou = property::all()->sortByDesc('created_at');

		return view('dashboardProperties',[
			'propertiesByYou' => $propertiesByYou
		]);
    }

	public function getUpdateProperty($pid) {
		$property = property::find($pid);
		$staff = User::all();
		return view('updateProperty',[
			'property' => $property,
			'staff' => $staff
		]);
    }

	public function getAddProperty() {

    	$staff = User::all();
    	return view('addProperty',[
    		'staff' => $staff
	    ]);
    }

	public function postAddProperty( Request $request ) {

    	if($request->hasFile('images')) {
		    $property              = new property();
		    $property->title       = $request->input( 'title' );
		    $property->description = $request->input( 'description' );
		    $property->price       = $request->input( 'price' );
		    $property->purpose     = $request->input( 'purpose' );
		    $property->type        = $request->input( 'type' );
		    $property->lga         = $request->input( 'lga' );
		    $property->city        = $request->input( 'city' );
		    $property->state        = $request->input( 'state' );
		    $property->address     = $request->input( 'address' );
		    $property->map         = $request->input( 'map' );
		    $property->isPublished = 1;
		    $property->uid         = $request->input('uid');
		    $property->save();


		    foreach ( $request->file( 'images' ) as $item ) {
			    $rand          = Str::random( 5 );
			    $inputFileName = $item->getClientOriginalName();
			    $item->move( "userUploads", $rand . $inputFileName );

			    $image      = new image();
			    $image->url = url( 'userUploads/' . $rand . $inputFileName );
			    $image->pid = $property->pid;
			    $image->save();
		    }

		    $request->session()->flash( 'success', 'Property added successfully' );

	    } else {
    		$request->session()->flash('error','You need to add at least one image.');
	    }
		return redirect('/add-property');

	}


	public function postUpdateProperty( Request $request ) {

    	$pid = $request->input('pid');


			$property              = property::find($pid);
			$property->title       = $request->input( 'title' );
			$property->description = $request->input( 'description' );
			$property->price       = $request->input( 'price' );
			$property->purpose     = $request->input( 'purpose' );
			$property->type        = $request->input( 'type' );
			$property->lga         = $request->input( 'lga' );
			$property->city        = $request->input( 'city' );
			$property->state        = $request->input( 'state' );
			$property->address     = $request->input( 'address' );
			$property->map         = $request->input( 'map' );
			$property->isPublished = 1;
			$property->uid         = $request->input('uid');
			$property->save();

		if($request->hasFile('images')) {
			foreach ( $request->file( 'images' ) as $item ) {
				$rand          = Str::random( 5 );
				$inputFileName = $item->getClientOriginalName();
				$item->move( "userUploads", $rand . $inputFileName );

				$image      = new image();
				$image->url = url( 'userUploads/' . $rand . $inputFileName );
				$image->pid = $property->pid;
				$image->save();
			}
		}
			$request->session()->flash( 'success', 'Property updated successfully' );

		return redirect('/property/'. $pid . '/update');

	}



	public function viewStaff() {
    	$staff = User::all();
		return view('viewStaff',[
			'staff' => $staff
		]);
	}

	public function deleteImage( Request $request, $imid ) {
    	$image = image::find($imid);

    	$exploded = explode('/', $image->url);
    	$filename = $exploded[count($exploded) - 1 ];

		try{
		    $adapter    = new Local( __DIR__ . '\..\..\..\\');
		    $filesystem = new Filesystem( $adapter );

		    $filesystem->delete( 'public/userUploads/' . $filename );
	    } catch (Exception $e){}


	    $status = image::destroy($imid);

    	return $status ;
	}

	public function deleteStaff( Request $request , $uid) {
		$staff = User::find($uid);

		$properties = property::where('uid', $uid)->update(['uid' => 1]);
		$status = $staff->delete();

		if($status)
		    $request->session()->flash('success','Staff deleted');
		else
		    $request->session()->flash('error','Sorry an error occurred');

		return redirect('view-staff');
	}

	public function deleteProperty( Request $request, $pid ) {
		$property = property::find($pid);
		$property->isPublished = 0;
		$property->save();

		$request->session()->flash('success','Property Deleted');

		return redirect('/');
	}

	public function logout() {
		Auth::logout();
		return redirect('/');
	}
}
