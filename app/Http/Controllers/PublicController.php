<?php

namespace App\Http\Controllers;

use App\image;
use App\property;
use App\User;
use Faker\Provider\UserAgent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PublicController extends Controller
{
	public function index() {
		if(User::all()->count() > 4){
			$users = User::all()->random(4);
		} else {
			$users = User::all();
		}

		$properties = property::all()->sortByDesc('created_at')->where('isPublished',1)->take(6);

		$recentProperties = property::all()->sortByDesc('created_at')->where('isPublished',1)->take(3);

		$cities = array();

		foreach ($properties as $item){
			array_push($cities,$item->city);
		}

		$cities = array_unique($cities);

		$type = array();

		foreach ($properties as $item){
			array_push($type,$item->type);
		}

		$type = array_unique($type);


		return view('welcome',[
			'properties' => $properties,
			'recentProperties' => $recentProperties,
			'users' => $users,
			'cities' => $cities,
			'type' => $type
		]);
    }

	public function team( ) {

		return view('team');
    }

	public function propertySearch() {


		if(property::where('isPublished',1)->count() > 4){
			$properties = property::all()->random(4);
		} else {
			$properties = property::where('isPublished',1)->get();
		}

		if(Input::has('term')){
			$term = Input::get('term');
			$properties = property::where('title','LIKE', "%$term%")->get();
		}
		$cities = array();

		foreach ($properties as $item){
			array_push($cities,$item->city);
		}

		$cities = array_unique($cities);

		$type = array();

		foreach ($properties as $item){
			array_push($type,$item->type);
		}

		$type = array_unique($type);


		return view('propertySearch',[
			'properties' => $properties,
			'cities' => $cities,
			'type' => $type
		]);
    }

	public function about() {
		return view('about');
    }

	public function services() {
		return view('services');
    }

	public function agents() {
		$agents = User::all();
		return view('agent',[
			'agents' => $agents
		]);
    }

	public function agent($uid) {
		$agent = User::find($uid);
		return view('agent',[
			'agent' => $agent
		]);
	}


	public function contact() {
		return view('contact');
    }

	public function viewProperty( $pid ) {

		if(property::where('isPublished',1)->count() > 4){
			$properties = property::all()->random(4);
		} else {
			$properties = property::where('isPublished',1)->get();
		}

		$property = property::find($pid);

		if($property->isPublished == 0) return redirect('/');

		$property->views = $property->views + 1;
		$property->save();

		$images = image::where('pid',$pid)->get();

		$requiredImages = array();
		foreach($images as $item){
			$image = ['src' => $item->url,'w' =>1200, 'h' =>900  ];
			array_push($requiredImages,$image);
		}

		$requiredImages = json_encode($requiredImages,JSON_UNESCAPED_SLASHES);


		return view('viewProperty',[
			'property' => $property,
			'images' => $images,
			'ajaxImages' => $requiredImages,
			'properties' => $properties
		]);
    }
}
