<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

//Public routes

Route::get('/', 'PublicController@index');
Route::get('/about','PublicController@about');
Route::get('/services','PublicController@services');
Route::get('/team','PublicController@team');
Route::get('/contact','PublicController@contact');
Route::get('/property-search','PublicController@propertySearch');
Route::get('/property/{pid}','PublicController@viewProperty');
Route::get('/agent/{uid}','PublicController@agent');


//Private routes

Route::get('/dashboard', 'HomeController@index')->name('home');
Route::get('/view-staff','HomeController@viewStaff');
Route::get('/property/{pid}/update','HomeController@getUpdateProperty');
Route::get('/delete-image/{imid}','HomeController@deleteImage');
Route::get('/delete-staff/{uid}','HomeController@deleteStaff');
Route::get('/delete/{pid}','HomeController@deleteProperty');
Route::get('/add-property','HomeController@getAddProperty');
Route::get('/view-properties','HomeController@dashboardProperties');
Route::get('/logout','HomeController@logout');

Route::post('/add-property','HomeController@postAddProperty');
Route::post('/property/{pid}/update','HomeController@postUpdateProperty');
