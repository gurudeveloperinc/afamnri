<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

    	Schema::create('properties', function(Blueprint $table){
    	    $table->increments('pid');
    	    $table->string('title');
    	    $table->string('description', 5000);
    	    $table->enum('purpose',['Sale','Rent']);
    	    $table->string('type');
    	    $table->string('lga')->nullable();
    	    $table->string('city');
    	    $table->string('state');
    	    $table->string('address',2000);
    	    $table->string('map',5000);
    	    $table->decimal('price');
    	    $table->integer('views')->default('0');
    	    $table->boolean('isPublished');
    	    $table->string('uid');
    	    $table->timestamps();
    	});

    	Schema::create('images', function(Blueprint $table){
    	    $table->increments('imid');
    	    $table->string('url',2000);
    	    $table->integer('pid');
    	    $table->timestamps();
    	});

        Schema::create('users', function (Blueprint $table) {
            $table->increments('uid');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('image',2000);
	        $table->string('description');
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

	    Schema::create('password_resets', function (Blueprint $table) {
		    $table->string('email')->index();
		    $table->string('token');
		    $table->timestamp('created_at')->nullable();
	    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('images');
	    Schema::dropIfExists('password_resets');
    }
}
