@extends('layouts.app')

@section('content')


    <section>
        <div class="row agentDetails">
            <div class=".col-md-6">
                <div class="white-box">

                <div class="titles1 col-md-12 col-sm-12 col-xs-12">
                    <h3>Agent Details</h3>
                </div>

                <div class="col-md-6">
                    <img class="img img-rounded" style="margin: 20px" src="{{$agent->image}}">
                </div>

                <div class="col-md-6">
                    <div class="row">
                        <b class="col-md-4">Name:</b>
                        <p class="col-md-6">{{$agent->name}} </p>

                    </div>

                    <div class="row">
                        <b class="col-md-4">Phone:</b>
                        <p class="col-md-6">{{$agent->phone}} </p>

                    </div>

                    <div class="row">
                        <b class="col-md-4">Email:</b>
                        <p class="col-md-6">{{$agent->email}} </p>

                    </div>

                    <div class="row">
                        <b class="col-md-4">Bio:</b>
                        <p class="col-md-8" style="line-height: 20px; font-size: 18px;">{{$agent->description}} </p>

                    </div>

                </div>

            </div>
        </div>
    </div>
    </section>
@endsection