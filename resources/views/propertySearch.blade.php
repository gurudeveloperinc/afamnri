@extends('layouts.app')

@section('content')

    <section>


        <!-- Start Property Section -->
        <div class="container">
            <div class="divider-medium"></div>
            <div class="row">
                <div class="col-md-8 col-sm-7">
                    <!-- Property View Mode Grid/List -->
                    <div class="listing-style-header clearfix">
                        <div class="view-mode">
                            <span>View Mode:</span>
                            <ul>
                                <li data-view="grid-style" class="property-list active"><i class="fa fa-th"></i></li>
                                <li data-view="list-style" class="property-list"><i class="fa fa-th-list"></i></li>
                            </ul>
                        </div>
                    </div>
                    <!-- Start Gallery Content  -->
                    <div id="property-list" class="grid-style">
                        <div class="row">
                            @foreach($properties as $item)
                            <div class="col-md-6 property-item">
                                <div class="property-box">
                                    <div class="property-image">
                                        <a href="{{url('property/' . $item->pid)}}">
                                            <span class="btn btn-default"><i class="fa fa-file-o"></i> Details</span>
                                        </a>
                                        <img alt="" src="{{$item->Images[0]->url}}">
                                    </div>
                                    <div class="property-price">
                                        <i class="fa fa-home"></i>For {{$item->purpose}}
                                        <span>NGN{{number_format($item->price)}}</span>
                                    </div>
                                    <div class="property-detail">
                                        <h3>
                                            <a href="{{url('property/' . $item->pid)}}">{{$item->title}}</a>
                                            <small>{{$item->address}}</small>
                                        </h3>
                                        <p>{{$item->description}}</p>

                                        <ul class="amenities">
                                            <li><i class="fa fa-eye" aria-hidden="true"></i> {{$item->views}}</li>
                                            <li>
                                                <a href="{{url('property/' . $item->pid)}}" class="btn-default btn-orange">View</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            @endforeach

                        </div>
                    </div><!-- End Gallery Content  -->
                    <div class="pagination">
                        <ul>
                            <li id="previous"><a href="#"><i class="fa fa-chevron-left"></i></a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li id="next"><a href="#"><i class="fa fa-chevron-right"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- Start Search Filter Sidebar -->
                <div class="col-md-4 col-sm-5 col-xs-12">
                    <div class="sidebar1">
                        <div class="titles1 col-md-12 col-sm-12 col-xs-12">
                            <h3>Search Property</h3>
                        </div>
                        <div class="search-section">


                            <form>
                                <div class="form-group">

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div class="select-news-det">

                                                <select name="city" >
                                                    <option value="null">Location</option>
                                                    @foreach($cities as $item)
                                                        <option>{{$item}}</option>
                                                    @endforeach
                                                </select>
                                            </div>


                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="select-news-det">
                                                    <select name="prop_type">
                                                        <option value="null">Type of property</option>
                                                        @foreach($type as $item)
                                                            <option>{{$item}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="select-news-det">
                                                    <select name="min_price">
                                                        <option value="">Min. Price</option>
                                                        <option value="25000">₦25000</option>
                                                        <option value="50000">₦50000</option>
                                                        <option value="75000">₦75000</option>
                                                        <option value="100000">₦100000</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="select-news-det">
                                                    <select name="max_price">
                                                        <option value="">Max. Price</option>
                                                        <option value="25000">₦25000</option>
                                                        <option value="50000">₦50000</option>
                                                        <option value="75000">₦75000</option>
                                                        <option value="100000">₦100000</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <p class="center">
                                    <button type="submit" class="btn btn-default-color">Search</button>
                                </p>
                            </form>
                        </div>
                        <div class="titles1 col-md-12 col-sm-12 col-xs-12">
                            <h3>Similar Property</h3>
                        </div>
                        <div class="widget_content">
                            <div class="listing-property">

                                @foreach($properties as $item)
                                    <div class="listing-item">
                                        <div class="sidebar-property">
                                            <div class="property-image">
                                                <a href="{{url('property/' . $item->pid)}}">
                                                    <span class="btn btn-default">View</span>
                                                </a>
                                                <img src="{{$item->Images[0]->url}}" alt="{{$item->title}}">
                                            </div>

                                            <div class="properties_info">
                                                <a href="{{url('property/' . $item->pid)}}">{{$item->address}}</a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div><!-- End Search Filter Sidebar -->
            </div>
            <div class="divider-medium"></div>
        </div><!-- End Property Section -->
    </section>

@endsection