@extends('layouts.app')

@section('content')

    <!-- Start FAQ Section -->
    <div class="container faq">
        <div class="divider-medium"></div>

        <h3 align="center">Our Services</h3>
        <p align="center" style="font-size: 18px; padding-bottom: 10px">We offer a range of professional services which we believe will be of benefit to your organization. Our services include but are not limited to the following:.</p>
        <div class="accordions toggle_style" data-attr="accordion3" id="accordion3">
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                       VALUATIONS	</a>
                </div>
                <div class="toggle_inner toggle-content">

                    <ul>
                        <li> 	Land and Building</li>
                        <li> 	Plant and Machinery</li>
                        <li>	Motor Vehicles</li>
                        <li>	Office and Household furniture</li>
                        <li>	Trade stocks</li>
                        <li>	Hotels</li>
                        <li>	Filling Stations</li>
                        <li>	Livestock and Farmland</li>

                    </ul>


                </div>
            </div>
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        PROJECT MANAGEMENT</a>
                </div>
                <div class="toggle_inner toggle-content">
                    <p>Project management is the overall planning, control and coordination of a project from conception to completion aimed at meeting a client’s requirements and ensuring completion on time within cost budget and to the required quality standard.
                        Our project management team aims at fully delivering satisfactory projects regarding quality, performance, timing and cost-in-use. We encourage, coordinate and motivate all participants to achieve project completion on programme and within budget.
                        .</p>
                    <p> Our range of services here includes:
                        i.	Infrastructure management
                        ii.	Facility management, etc
                    </p>
                </div>
            </div>
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">

                        PROJECT/ ESTATE DEVELOPMENT
                    </a>
                </div>
                <div class="toggle_inner toggle-content">
                    <p>i.	Land Reform<br>
                        ii.	Land Registration<br>
                        iii.	Estate Planning<br>
                        iv.	Estate Analysis
                    </p>
                </div>
            </div>
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">

                        ESTATE AGENCY
                    </a>
                </div>

                <div class="toggle_inner toggle-content">
                    <p> We could act as your agent in.</p>
                    <p>i.	Property sales
                        <br>ii.	Property leases
                    </p>
                </div>

            </div>
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">

                        PROPERTY MANAGEMENT
                    </a>
                </div>

                <div class="toggle_inner toggle-content">
                    <p> We could act as your agent in.</p>
                    <p>i.	Negotiation of terms and conditions of the tenants
                        <br>ii.	Disbursement of outgoings
                        <br>iii.	Advice on adequate insurance policy
                        <br>iv.	Collection of rents
                        <br>v.	    Liaising with clients’ solicitors in advising in an appropriate lease        agreement

                    </p>
                </div>

            </div>
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">

                        PROPERTY FINANCE
                    </a>
                </div>

                <div class="toggle_inner toggle-content">
                    <p> Property finance refers to the process of obtaining funds or capital, generally for the purpose of supporting development and re-development or carrying out major renovation works on property development. Here, you may require us to estimate mortgage value of the property in order to secure a loan or to advice you on the best source of finance for project options.</p>

                </div>

            </div>
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">

                        FEASIBILITY & VIABILITY STUDIES
                    </a>
                </div>

                <div class="toggle_inner toggle-content">
                    <p> Our firm prepares feasibility studies. This is to determine whether a scheme as designed is viable or not. This would do investors a lot of good as potential losses are avoided.</p>

                </div>

            </div>
        </div>
        <div class="divider-medium"></div>
    </div>


@endsection