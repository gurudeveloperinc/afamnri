@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12" style="margin-top:-20px;">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3940.320033056279!2d7.495087014440258!3d9.034542691390586!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x104e0b8e0c6611cb%3A0xb1ebd2225c6cc671!2sAfam+Nri+Consulting!5e0!3m2!1sen!2sng!4v1499812898312" style="width:100%; height: 300px;" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

        <section>
            <div class="contact">
                <!-- Contact map -->

                <div class="contact-bg show-block">
                    <div class="container contact-container" style="margin: 20px auto">
                        <div class="property-contact-box">
                            <!-- Contact Form -->
                            <form id="contact_form" class="contact-form">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="contact-title">
                                                    <h2>Get in touch!</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-8 col-sm-7 col-xs-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" required="" placeholder="Your Name" id="name" name="name">
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <input type="text" required="" placeholder="Your Email" id="email" name="email">
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <input type="text" required="" placeholder="Subject" id="phone" name="phone">
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                    <textarea required="" placeholder="Type your message here ..." rows="8" cols="8"
                                              id="message" name="message"></textarea>
                                            </div>
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <button class="contact-agent-button" id="submit">
                                                    <i class="fa fa-paper-plane"></i> Get In Touch
                                                </button>
                                            </div>
                                            <span class="message-contact"></span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-5 col-xs-12">
                                        <div class="contact-web-info">
                                            <h1>AFAM NRI CONSULTING  </h1>
                                            <p><b>Chartered Real Estate Surveyors </b></p>
                                        </div>
                                        <div class="contact-website-info">
                                            <p >For service excellence, enhanced cohesion and better portfolio management, we are organized along Real Estate Surveying and Valuation for all purposes </p>
                                            <p><b>www.afamnriconsulting.com.ng</b></p>
                                        </div>
                                        <ul class="property-address-block">
                                            <li>
                                                <div class="one_third"><i class="fa fa-map-marker"></i> Head Office:</div>
                                                <div class="two_third">Suite C12, Second Floor, Al-Maliha Plaza,
                                                    Opposite KIA Motors, Area 11, Garki, Abuja
                                                </div>
                                            </li>
                                            <li>
                                                <div class="one_third"><i class="fa fa-phone-square"></i> Phone:</div>
                                                <div class="two_third">08187191304</div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div><!-- End Contact Section -->

        </section>
        <div class="divider-small"></div>
        <div class="col-sm-12">
            <div class="white-box">
                <h3 class="box-title m-b-0">Contact Us</h3>
                <p class="text-muted m-b-30">Please contact us at any of our offices listed below, or use the Contact Us form below to send us a quick email.</p>
                <table data-toggle="table" data-height="250" data-mobile-responsive="true" class="table-striped">
                    <thead>
                    <tr>
                        <th>Branch</th>
                        <th></th>
                        <th>Information</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr id="tr-id-1" class="tr-class-1">
                        <td> <b>Abuja | </b><br>

                            Corporate Office:Suite C12, Second Floor, Al-Maliha Plaza,<br>
                            Opposite KIA Motors, Area 11, Garki, Abuja.
                        </td>
                        <td><br>    </td>
                        <td><b>Tel:</b> 08187191304, 08092708822 <br>
                            <b>E-mail:</b>info@afamnriconsulting.com.ng
                        </td>
                    </tr>
                    <tr id="tr-id-2" class="tr-class-2">
                        <td>  <b>PortHarcourt | </b><br>
                            No 28 Nyenwenwo Avenue By Pentecost Close,<br>
                            Eliada Layout/Cocaine Estate,
                            Rumuogba, Port Harcourt.
                        </td>
                        <td><br>      </td>
                        <td><b>Tel:</b> 08092728822 , 09098629816 <br>
                            <b>E-mail:</b>portharcourt@afamnriconsulting.com.ng  </td>
                    </tr>
                    <tr id="tr-id-3" class="tr-class-3">
                        <td> <b>Anambra | </b> <br>
                            Suite B6, Millineum Plaza,
                            Ziks, Avenue, Awka <br>Anambra State
                        </td>
                        <td><br>     </td>
                        <td><b>Tel:</b> 08035884423, 09099599746<br>
                            <b>E-mail:</b>awka@afamnriconsulting.com.ng  </td>
                    </tr>

                    <tr id="tr-id-4" class="tr-class-4">
                        <td> <b>Lagos | </b> <br>
                            Suit 4, Ikoyi building, Lagos State.
                        </td>
                        <td><br>     </td>
                        <td><b>Tel:</b> 07062512262<br>

                    </tr>



                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection