@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        @include('notification')
        <div class="col-md-10 col-md-offset-1 dashboard">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div align="center" class="panel-body">
                    <div class="row">
                        <div class="col-md-6">
                            <h3>Total Properties</h3>
                            <p  style="font-size:30px">{{count($properties)}}</p>
                        </div>

                        <div class="col-md-6">
                            <h3>Total Staff</h3>
                            <p  style="font-size:30px">{{count($staff)}}</p>
                        </div>
                    </div>

                    <br>
                    <div class="clearfix"></div>

                    <div class="row" align="center">
                        <a href="{{url('/register')}}" class="btn btn-primary">Add Staff</a>
                        <a href="{{url('/view-staff')}}" class="btn btn-primary">View Staff</a>
                        <a href="{{url('/add-property')}}" class="btn btn-primary">Add Property</a>
                        <a href="{{url('/view-properties')}}" class="btn btn-primary">View Properties</a>
                    </div>

                    <div class="row">
                        <br><br>
                        <h3 align="center">Last 5 Properties Added</h3>
                        <table class="table table-responsive">
                            <tr>
                                <th>Title</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Purpose</th>
                                <th>Price</th>
                                <th></th>
                                <th></th>
                            </tr>

                            <?php $count = 0; ?>
                            @foreach($properties as $property)
                                @if($count < 5)
                                <tr>
                                    <td>{{$property->title}}</td>
                                    <td>{{$property->city}}</td>
                                    <td>{{$property->state}}</td>
                                    <td>{{$property->purpose}}</td>
                                    <td>{{number_format($property->price)}}</td>
                                    <td>
                                        <a href="{{url('/property/' . $property->pid)}}">View</a>
                                    </td>
                                    <td>
                                        <a href="{{url('/property/' . $property->pid . '/update')}}">Update</a>
                                    </td>
                                </tr>
                                @endif
                                <?php $count++; ?>
                            @endforeach
                        </table>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
