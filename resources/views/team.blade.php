@extends('layouts.app')

@section('content')


    <!-- Start TEAM Section -->
    <div class="container faq">
        <div class="divider-medium"></div>

        <h3 align="center">Our Team</h3>
        <p align="center" style="font-size: 18px; padding-bottom: 10px">Below is a list of our team members</p>
        <div class="accordions toggle_style" data-attr="accordion3" id="accordion3">
            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        AFAM CHARLES NRI - PRINCIPAL PARTNER	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>

                        He started his career with Zenith Bank Plc where he worked in Risk Management department before proceeding to Clem Eze & Co where he rose to the exalted position of General Manager of the firm before establishing the firm of AFAM NRI CONSULTING.
                        Afam has gone for several courses both home and abroad. He has post graduate diploma in Estate Management. He is an Associate member of Nigeria Institution of Estate Surveyors & Valuers and registered with Estate Surveying Registration Board of Nigeria.

                    </p>


                </div>
            </div>



            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        SOLOMON UWALEME (BSc. Est. Man., Associate NIM)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        He is an Associate Partner in AFAM NRI CONSULTING. He started his career with Polly Nwoke and Co in 2009; where he rose to become the head of the management team in the Abuja Office and later joined the firm AFAM NRI CONSULTING

                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        MARGRET CHIOMA NWOSU HND (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        She holds a Higher National Diploma in Estate Management from Federal Polytechnic Nekede, Owerri, Imo State. She started her career with Okereke Uduak & Partners as Surveyors in training in 2010. She later joins Nwokoma Nwankwo & Co. in their management Department and presently the manager in AFAM NRI CONSULTING she has gained useful experience through training and courses.

                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        ACHIBIRI CHARLES (EST. MGT)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>

                        Achibiri Charles is the head of valuation in AFAM NRI CONSULTING, Abuja branch. He had worked with D’Lord Maseli & Partner, as well as Jide Taiwo & Co. Charles has a Bsc in Estate Management.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        ACHEM CHUBI SAMSON (Est. Mgt)</a>
                </div>
                <div class="toggle_inner toggle-content">

                    <p>
                        He is the head of the Agency team in AFAM NRI CONSULTING. He started his career with Urban Shelter Facility Management Company in 2014.He has a B.Tech in Estate Management and Valuation from Federal University of Technology Minna. He has earned some experience through trainings and courses.

                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        OLADIMEJI JUMOKE (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        She started her career with OWOEYE AND PARTNERS and now with AFAM NRI CONSULTING; she is part of the management team in Abuja Branch. Jumoke has HND in Estate Management from Federal Polytechnic Nassarawa State.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        AVADOO SHIMAOR (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        She started her career with AFAM NRI CONSULTING; she has B.Tech in Estate Management from Cross Rivers State University.

                    </p>


                </div>
            </div>



            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        PAUL ODUM (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        He has BSC in Estate Management from University Of Nigeria Nsukka, Enugu State. He has earned some experience through trainings and courses. He is currently working with Afam Nri Consulting.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        CHIOMA OKAFOR (Est Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>

                        She has BSC in Estate Management from Nnamdi Azikiwe University, Awka , Anambra State. She has earned some experience through trainings and courses. She is currently working with Afam Nri Consulting.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        UZODINMA OKEZIE (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        He has BSC in Estate Management from Obafemi Awolowo University, Ile Ife, Osun State. He started his career with Jide Taiwo & Co and has earned some experience through trainings and courses. He is currently working with Afam Nri Consulting.
                    </p>


                </div>
            </div>


            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        EMMANUEL UGWU (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        He has BSC in Estate Management from Enugu State University Of Science And Technology, Enugu State. He started his career with Afam Nri Consulting and has earned some experience through trainings and course.
                    </p>


                </div>
            </div>


            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        ONYEKACHUKWU IGWEZE (Est Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        She has BSC in Estate Management from Covenant University; she has earned some experience through trainings and courses. She is currently working with Afam Nri Consulting.

                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        ONYEKACHI NZOMIWU (Est. Mgt)	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        He has BSC in Estate Management from Nnamdi Azikiwe University, Awka , Anambra State. he has earned some experience through trainings and courses. He is currently working with Afam Nri Consulting.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        NANCY CHUKWUEMEKA (Est. Mgt)</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        She has HND in Estate Management from Federal Polytechnic Nekede. Owerri. She has earned some experience through trainings and courses. She is currently working with Afam Nri Consulting.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        Chika Onuoha	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        She is the head of Administration in AFAM NRI CONSULTING. She sees to the comportment of staff to conform to the policy and objectives of the firm.
                    </p>


                </div>
            </div>

            <div class="toggle toggle_group">
                <div class="toggle-title toggle_heading">
                    <a href="#" class="accordion-toggle">
                        BARR. OKOOBO OSAJILI	</a>
                </div>
                <div class="toggle_inner toggle-content">


                    <p>
                        He prepares all the company legal documents, advices the company in terms of legal issues as well as represents the company from time to time as the need arises.
                    </p>


                </div>
            </div>

        </div>




        <div class="divider-medium"></div>
    </div>



    {{--<div class="row">--}}
    {{--<div class=".col-md-6">--}}
    {{--<div class="white-box">--}}
    {{--<h3 class="box-title" align="center">Team List</h3>--}}
    {{--<p align="center" style="font-size: 16px; width:60%; margin:auto; margin-top:10px; margin-bottom:30px;">Our professional departments are manned by qualified and experienced Estate Surveyors and Valuers supported by equally experienced and efficient administrative and technical staff.</p>--}}
    {{--<div class="table-responsive">--}}
    {{--<table class="table">--}}
    {{--<thead>--}}
    {{--<tr>--}}
    {{--<th>#</th>--}}
    {{--<th>Name</th>--}}
    {{--<th>Role</th>--}}

    {{--</tr>--}}
    {{--</thead>--}}
    {{--<tbody>--}}
    {{--<tr>--}}
    {{--<td>1</td>--}}
    {{--<td>Afam charles nri</td>--}}
    {{--<td>principal partner</td>--}}


    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>2</td>--}}
    {{--<td>Solomon uwaleme</td>--}}
    {{--<td>associate partner</td>--}}


    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>3</td>--}}
    {{--<td>Margret chioma nwosu	</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>4</td>--}}
    {{--<td>Achibiri charles</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>5</td>--}}
    {{--<td>Jumoke oladimeji</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>6</td>--}}
    {{--<td>Avadoo shinaor</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>7</td>--}}
    {{--<td>Achem chubi samson</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>8</td>--}}
    {{--<td>Onyekachi igweze</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>9</td>--}}
    {{--<td>Chioma okafor</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>10</td>--}}
    {{--<td>Offor kelvin ifeanyi</td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>11</td>--}}
    {{--<td>Nancy chukwuemeka </td>--}}
    {{--<td>estate surveyor</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>12</td>--}}
    {{--<td>Ifunanya iwuoha </td>--}}
    {{--<td>head, admin</td>--}}

    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>13</td>--}}
    {{--<td>Barr. Okoobo osajili</td>--}}
    {{--<td>Company Lawyer</td>--}}

    {{--</tr>--}}
    {{--</tbody>--}}
    {{--</table>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}


    {{--</section>--}}
    {{--</div>--}}

@endsection