@extends('layouts.app')

@section('content')

    <section>

        <!-- Start Property Detail Section -->
        <div class="property-section">
            <div class="divider-medium"></div>
            <div class="container">
                <div class="row">
                    <!-- Start Property Content -->
                    <div class="col-md-8 col-sm-7 col-xs-12">
                        <h1 class="property-title title-properties-detail">{{$property->title}}
                            <small>{{$property->city}}</small>
                        </h1>
                        <!-- Short description about property -->
                        <div class="property-top">
                            <ul class="amenities">
                                <li><i class="fa fa-eye"></i>{{$property->views}}</li>

                                <li><i class="icon_building"></i> {{$property->type}}</li>

                            </ul>
                            <div id="property-id">ID: #{{$property->pid}}</div>
                        </div>
                        <!-- Start description about property -->
                        <div class="property-detail">
                            <div class="price">
                                <i class="fa fa-home"></i>For {{$property->purpose}}
                                <span>NGN{{number_format($property->price)}}</span>
                            </div>
                            <!-- Start Property Image Gallery -->
                            <div class="property-detail-img">
                                <div id="property-detail-img" class="owl-carousel">

                                    @foreach($images as $item)
                                    <div class="item">
                                        <img src="{{$item->url}}" alt="">
                                    </div>
                                    @endforeach


                                </div>
                            </div>
                            <div class="property-thumbs-img">
                                <div id="property-thumbs-img" class="owl-carousel">
                                    @foreach($images as $item)

                                    <div class="item">
                                        <div class="thumb-image">
                                            <img src="{{$item->url}}" alt="">
                                        </div>
                                    </div>
                                    @endforeach

                                </div>
                            </div><!-- End Property Image Gallery -->
                            <h1 class="property-title">DESCRIPTION</h1>
                            <p>
                                {{$property->description}}

                            </p>
                        </div><!-- End description about property -->

                        <!-- property detail content -->
                        <h1 class="property-title">LOCATION</h1>
                        <div class="property-feature-details">
                            {!!$property->map!!}
                        </div>


                        <!-- Assigned Agent Detail -->
                        <h1 class="property-title">Agent</h1>
                        <div class="property-worker-list">
                            <div class="worker-photo">
                                <a href="{{url('agent/' . $property->Agent->uid )}}" class="property-worker-photo">
                                    <img class="img img-thumbnail" src="{{$property->Agent->image}}" alt="{{$property->Agent->name}}">
                                    <figure class="property-worker-photo-hover">
                                        <span class="property-worker-photo-more">View Details</span>
                                    </figure>
                                </a>
                            </div>
                            <div class="property-worker-intro">
                                <div class="property-worker-intro-head">
                                    <div class="property-worker-intro-name">
                                        <h3 class="worker-name">{{$property->Agent->name}} </h3>
                                        <div class="worker-post"></div>
                                    </div>
                                </div>
                                <button type="button" class="worker-show">Contact agent</button>
                                <div class="property-worker-intro-row">
                                    <div class="property-worker-intro-col">

                                        <div class="tel"><span>Mobile:</span><br><a href="tel:+2348187191304">+2348187191304</a>, <a href="tel:{{$property->Agent->phone}}">{{$property->Agent->phone}}</a>

                                            <div class="tel">
                                                {{--<span>Mob.</span><a href="tel:+44(0)30345207210">233657908</a>--}}
                                            </div>
                                            <div class="email"><span>Email:</span><a
                                                        href="mailto:{{$property->Agent->email}}">{{$property->Agent->email}}</a></div>

                                        </div>
                                    </div>
                                </div>
                                <div class="property-worker-intro-row">
                                    <div class="property-worker-descr">
                                        <p>
                                            {{$property->Agent->description}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <!-- Contact form detail -->
                        <form action="#" method="POST" class="property-agent-form" id="property-agent-form">
                            <div class="row">
                                <div class="form-group col-md-4 col-xs-12 required">
                                    <label for="in-form-name" class="form-control-label">Your Name</label>
                                    <input id="in-form-name" type="text" name="name" required="" class="form-control">
                                </div>
                                <div class="form-group col-md-4 col-xs-12">
                                    <label for="in-form-phone" class="form-control-label">Telephone</label>
                                    <input id="in-form-phone" type="text" name="phone" class="form-control">
                                </div>
                                <div class="form-group col-md-4 col-xs-12 required">
                                    <label for="in-form-email" class="form-control-label">E-mail</label>
                                    <input id="in-form-email" type="email" name="email" required="" class="form-control">
                                </div>
                                <div class="form-group col-md-12 col-xs-12 required">
                                    <label for="in-form-message" class="form-control-label">Message</label>
                                    <textarea id="in-form-message" name="message" required="" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <button type="submit" class="form-submit">Submit</button>
                            </div>
                        </form>
                        <!-- Start Similar Property section -->
                        <h1 class="property-title">Similar Properties</h1>
                        <div id="similar-properties" class="grid-style clearfix">
                            <div class="row">

                                @foreach($properties as $item)
                                    <div class="property-item col-md-6">
                                    <div class="property-box">
                                        <div class="property-image">
                                            <a href="{{url('property/' . $item->pid)}}">
                                                <h3>{{$item->title}}</h3>
                                                <span class="location">{{$item->address}}</span>
                                            </a>
                                            <img src="{{$item->Images[0]->url}}" alt="">
                                        </div>
                                        <div class="property-price">
                                            <i class="fa fa-home"></i>For {{$item->purpose}}
                                            <span>NGN{{number_format($item->price)}}</span>
                                        </div>
                                        <div class="property-detail">
                                            <ul class="amenities">
                                                <li><i class="fa fa-eye" aria-hidden="true"></i> {{$item->views}}</li>
                                                <li><i class="fa fa-home" aria-hidden="true"></i> {{$item->type}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div><!-- End Similar Property section -->
                    </div><!-- End Property Content -->
                    <!-- Start Filter Search Sidebar -->
                    <div class="col-md-4 col-sm-5 col-xs-12">

                        <div class="sidebar1">
                            <div class="titles1 col-md-12 col-sm-12 col-xs-12">
                                <h3>Details</h3>
                            </div>
                            <div class="search-section" style="padding-bottom: 150px;">
                                <a style="margin-left: 30px;" class="btn btn-primary" id="openGallery">Expand Gallery</a>

                                <div class="row">
                                    <b class="col-md-4">Address:</b>
                                    <p class="col-md-8">{{$property->address}} </p>
                                </div>

                                <div class="row">
                                    <b class="col-md-4">Purpose:</b>
                                    <p class="col-md-6">{{$property->purpose}} </p>
                                </div>

                                <div class="row">
                                    <b class="col-md-4">Type:</b>
                                    <p class="col-md-6">{{$property->type}} </p>
                                </div>

                                <div class="row">
                                    <b class="col-md-4">LGA:</b>
                                    <p class="col-md-6">{{$property->lga}} </p>
                                </div>

                                <div class="row">
                                    <b class="col-md-4">City:</b>
                                    <p class="col-md-6">{{$property->city}} </p>
                                </div>

                                <div class="row">
                                    <b class="col-md-4">State:</b>
                                    <p class="col-md-6">{{$property->state}} </p>
                                </div>

                                <div class="row">
                                <b class="col-md-4">Price:</b>
                                <p class="col-md-6">{{number_format($property->price)}} </p>
                                </div>

                                <div class="row">
                                    <b class="col-md-4">Views:</b>
                                    <p class="col-md-6">{{$property->views}} </p>
                                </div>

                                <br><br>

                                @if(!Auth::guest())

                                @if(Auth::user()->uid == $property->uid)
                                <div class="col-md-12">
                                <a href="{{url('/delete/' . $property->pid)}}" class="btn btn-danger">Delete</a>
                                </div>
                                @endif
                                @endif
                                <br><br>
                                <div class="titles1 col-md-12 col-sm-12 col-xs-12">
                                    <h3>Agent</h3>
                                </div>

                                <div class="row">
                                {{--<img style="width: auto; height: 200px;" class="img-thumbnail" src="{{$property->agent->image}}">--}}
                                </div>

                                <b class="col-md-4">Name:</b>
                                <p class="col-md-6">{{$property->Agent->name}} </p>

                                <b class="col-md-4">Phone:</b>
                                <p class="col-md-6">+2348187191304, {{$property->Agent->phone}} </p>

                                <b class="col-md-4">Email:</b>
                                <p class="col-md-6">{{$property->Agent->email}} </p>
                                </div>

                            </div>

                        <br><br>
                            <div class="titles1 col-md-12 col-sm-12 col-xs-12">
                                <h3>Other Properties</h3>
                            </div>
                            <div class="widget_content">
                                <div class="listing-property">

                                    @foreach($properties as $item)
                                        <div class="listing-item">
                                        <div class="sidebar-property">
                                            <div class="property-image">
                                                <a href="{{url('property/' . $item->pid)}}">
                                                    <span class="btn btn-default">View</span>
                                                </a>
                                                <img src="{{$item->Images[0]->url}}" alt="{{$item->title}}">
                                            </div>

                                            <div class="properties_info">
                                                <a href="{{url('property/' . $item->pid)}}">{{$item->address}}</a>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div><!-- End Filter Search Sidebar -->
                </div>
            </div>
            <div class="divider-medium"></div>
        </div><!-- End Property Detail section -->
    </section>
    <div class="divider-small"></div>



    <!-- Root element of PhotoSwipe. Must have class pswp. -->
    <div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">

        <!-- Background of PhotoSwipe.
             It's a separate element as animating opacity is faster than rgba(). -->
        <div class="pswp__bg"></div>

        <!-- Slides wrapper with overflow:hidden. -->
        <div class="pswp__scroll-wrap">

            <!-- Container that holds slides.
                PhotoSwipe keeps only 3 of them in the DOM to save memory.
                Don't modify these 3 pswp__item elements, data is added later on. -->
            <div class="pswp__container">
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
                <div class="pswp__item"></div>
            </div>

            <!-- Default (PhotoSwipeUI_Default) interface on top of sliding area. Can be changed. -->
            <div class="pswp__ui pswp__ui--hidden">

                <div class="pswp__top-bar">

                    <!--  Controls are self-explanatory. Order can be changed. -->

                    <div class="pswp__counter"></div>

                    <button class="pswp__button pswp__button--close" title="Close (Esc)"></button>

                    <button class="pswp__button pswp__button--share" title="Share"></button>

                    <button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>

                    <button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>

                    <!-- Preloader demo http://codepen.io/dimsemenov/pen/yyBWoR -->
                    <!-- element will get class pswp__preloader--active when preloader is running -->
                    <div class="pswp__preloader">
                        <div class="pswp__preloader__icn">
                            <div class="pswp__preloader__cut">
                                <div class="pswp__preloader__donut"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
                    <div class="pswp__share-tooltip"></div>
                </div>

                <button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)">
                </button>

                <button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)">
                </button>

                <div class="pswp__caption">
                    <div class="pswp__caption__center"></div>
                </div>

            </div>

        </div>

    </div>

    <script>

        $(document).ready(function(){
            window.Laravel.data = '<?php echo $ajaxImages; ?>';
            console.log(window.Laravel.data);
            var imageItems = JSON.parse(window.Laravel.data);

            $('.carousel').carousel();

            console.log(imageItems);

            function openGallery(){


                var pswpElement = document.querySelectorAll('.pswp')[0];

                // build items array
                var items = imageItems;

                // define options (if needed)
                var options = {
                    // optionName: 'option value'
                    // for example:
                    index: 0 // start at first slide
                };

                // Initializes and opens PhotoSwipe
                var gallery = new PhotoSwipe( pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();

            }


            $('#openGallery').on('click',openGallery);


        });

    </script>


@endsection