@extends('layouts.app')

@section('content')


    <!--/banner-section-->
    <div id="demo-1" data-zs-src='["img/homeSlider1.jpg", "img/homeSlider2.jpg", "img/homeSlider3.jpg"]' data-zs-overlay="dots">
        <div class="demo-inner-content">
            <!--/banner-info-->
            <div class="baner-info">
                <h3>Real Estate Surveying and Valuation</h3>
                <h4>Value Your Assets</h4>
            </div>
            <!--/banner-ingo-->
        </div>
    </div>
    <!--/banner-section-->
    </div>
    <!--/main-header-->
    <!-- /pop-->
    <div class="modal ab fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog sign" role="document">
            <div class="modal-content about">
                <div class="modal-header one">
                    <button type="button" class="close sg" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <div class="discount one">
                        <h3>A Few Words about Us</h3>

                    </div>
                </div>
                <div class="modal-body about">
                    <img src="images/v5.jpg" alt="w3ls" class="img-responsive">
                    <h4>Lorem ipsum Nam libero tempore</h4>
                    <p>Lorem ipsum Nam libero tempore cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere.Lorem Ipsum has been the industry's standard dummy text ever since.</p>

                </div>


            </div>
        </div>
    </div>
    <!-- //pop-->
    <!--/price-section-->
    <div class="price-section">
        <div class="container">
            <div class="col-md-6 price">
                <h3><span>Search For Property</span></h3>

                <form action="{{url('/property-search')}}">
                <div class="reservation">
                    <div class="section_room">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                        <select id="country" name="city" class="frm-field required">
                            <option value="null">Location</option>
                            @foreach($cities as $item)
                                <option>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="section_room">
                        <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                        <select id="country1" onchange="change_country(this.value)" name="type" class="frm-field required">
                            <option value="null">Type of property</option>
                            @foreach($type as $item)
                                <option>{{$item}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="reservation-grids">
                        <div class="reservation-grid-left">
                            <div class="section_room">
                                <span aria-hidden="true">₦</span>
                                <input type="text" name="min" placeholder="       Min Price" class="frm-field required" style="padding:12px">
                            </div>
                        </div>
                        <div class="reservation-grid-right">
                            <div class="section_room">
                                <span aria-hidden="true">₦</span>
                                <input type="text" name="max" placeholder="       Max Price" class="frm-field required" style="padding:12px">
                            </div>
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="keywords">

                            <input type="submit" value="Search">

                    </div>

                </div>
                </form>
            </div>
            <div class="col-md-6 plat">
                <div id="myCarousel1" class="carousel slide" data-ride="carousel" data-interval="2000" data-pause="hover">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel1" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel1" data-slide-to="1"></li>
                        <li data-target="#myCarousel1" data-slide-to="2"></li>
                    </ol>
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">


                        <?php $count = 0; ?>
                        @foreach($recentProperties as $item)

                            <div class="item @if($count == 0) active @endif ">
                                <div class="serv-w3ls1">

                                    <img src="{{$item->Images[0]->url}}" alt="w3ls" class="img-responsive">
                                    <span class="four">NGN{{number_format($item->price)}}</span>
                                    <div class="rent-bottom">
                                        <a href="{{url('property/' . $item->pid)}}" >
                                        <h4>{{$item->title}}</h4>
                                        </a>
                                        <h5>For {{$item->purpose}}</h5>
                                        <p>{{$item->address}}<p>
                                        <div class="rent-icons">
                                            <h6 class="area">City: {{$item->city}}</h6>
                                            <div class="r-icons">
                                                <ul class="icons-right">
                                                    <li><i class="fa fa-eye" aria-hidden="true"></i> {{$item->views}}</li>
                                                    <li>
                                                        <a href="{{url('property/' . $item->pid)}}" class="btn-default btn-orange">View</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <?php $count++; ?>
                        @endforeach
                    </div>
                </div>

            </div>
            <div class="clearfix"> </div>
        </div>
    </div>

    <!--//price-section-->
    <!-- Start Recently Listed Section -->
    <div class="recently-property">
        <div class="divider-medium"></div>
        <div class="container">
            <div class="titles1">
                <h3>Recently listed Properties</h3>
                <p>Look at our latest listed properties and check out the facilities on them, We have already sold
                    more
                    than you think.
                    <a href="{{url('property-search')}}" rel="bookmark" class="btn-default btn-orange">Properties</a>
                </p>
            </div>
            <div class="property-container">

                    <?php $counter = 0; ?>
                    @foreach($properties as $item)


                    <div class="col-xs-12 col-lg-4 col-sm-6">
                        <article class="recently-property-post">
                            <div class="recently-property-thumbnail">
                                <a href="{{url('property/' . $item->pid)}}"><img width="660" height="600" src="{{$item->Images[0]->url}}"
                                                                    class="img-responsive1" alt=""></a>
                            </div>
                            <div class="recently-property-description">
                                <header class="entry-header">
                                    <h4 class="entry-title"><a href="{{url('property/' . $item->pid)}}" rel="bookmark">{{$item->title}}</a>
                                    </h4>
                                    <div class="price-status">
                                        <span class="price">NGN{{number_format($item->price)}}</span>
                                        <a href="{{url('property/' . $item->pid)}}"><span class="status-tag">For {{$item->purpose}}</span></a>
                                    </div>
                                </header>
                                <div class="property-meta-info">
                                    <div class="meta-item">
                                        <i class="icon_map"></i>
                                        <div>
                                            <span class="meta-item-title">City</span>
                                            <span class="meta-item-value">{{$item->city}}</span>
                                        </div>
                                    </div>
                                    <div class="meta-item meta-property-type">
                                        <i class="icon_house"></i>
                                        <div>
                                            <span class="meta-item-title">Type</span>
                                            <span class="meta-item-value">{{$item->type}}</span>
                                        </div>
                                    </div>
                                    <div class="meta-item">
                                        <i class="arrow_expand_alt3"></i>
                                        <div>
                                            <span class="meta-item-title">LGA</span>
                                            <span class="meta-item-value">{{$item->lga}}</span>
                                        </div>
                                    </div>
                                    <div class="meta-item">
                                        <i class="icon_book"></i>
                                        <div>
                                            <span class="meta-item-title">Views</span>
                                            <span class="meta-item-value">{{$item->views}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </article>
                    </div>

                    @endforeach

            </div>
        </div>
    </div>
    <div class="divider-medium"></div>

    <!-- End Recently Listed Section -->
    <div class="testimonial-item">
        <div class="review testimonial-review">
            <div class="">
                <div class="testimonial-review-photo">
                    <figure><img src="{{url('images2/afam.jpg')}}" alt="ALT"
                                 class="testimonial-review-photo-img">
                    </figure>
                </div>
                <!-- Testimonial Rating & Name -->

                <!-- Testimonial Description -->
                <p class="testimonial-review-info">AFAM NRI CONSULTING is a firm of Chartered Real Estate Surveyors and Valuers with bias in Property Valuation. The firm also undertakes Feasibility and Viability Appraisals/Development Consultancy, Facility Management, Real Estate Agency and Real Estate Management.

                    We are therefore affiliated with the Nigerian Institution of Estate Surveyors and Valuers (NIESV), and are duly registered with the Estate Surveyors and Valuers Registration Board of Nigeria (ESVRBON).

                    <br>  <a href="{{url('about')}}" rel="bookmark" class="btn-default btn-orange">Read More</a>
                </p>

            </div>
        </div>
    </div><!-- End Single Testimonial Item -->
    <div class="divider-small"></div>
    <!-- Start Agent Section -->
    <section>

        <div class="our-agent">
            <div class="divider-medium"></div>

            <div class="container">

                <div class="titles1">
                    <h3>Our Staff</h3>
                    <p>Our professional departments are manned by qualified and experienced Estate Surveyors and Valuers supported by equally experienced and efficient administrative and technical staff.
                    </p>
                </div>

                <div class="row">


                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <div class="agent1">
                            <a href="#" class="agent-profile-img">
                                <img src="{{url('img/chioma.jpg')}}" alt="Chioma Okafor">
                                <div class="ring-effect"></div>
                            </a>
                            <h3 class="agent-name">CHIOMA OKAFOR</h3>
                            <div>
                                <ul>
                                    <li><span class="fa fa-envelope" aria-hidden="true"></span> <p>chioma@afamnriconsulting.com.ng</p></li>
                                    <li><span class="fa fa-phone-square" aria-hidden="true"></span> 08033306134</li>

                                </ul>
                            </div>
                            <div class="agent-contact">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <div class="agent1">
                            <a href="#" class="agent-profile-img">
                                <img src="{{url('img/magret.jpg')}}" alt="Chioma Okafor">
                                <div class="ring-effect"></div>
                            </a>
                            <h3 class="agent-name">MARGRET CHIOMA NWOSU</h3>
                            <div>
                                <ul>
                                    <li><span class="fa fa-envelope" aria-hidden="true"></span> <p>margret@afamnriconsulting.com.ng</p></li>
                                    <li><span class="fa fa-phone-square" aria-hidden="true"></span>08119340897</li>

                                </ul>
                            </div>
                            <div class="agent-contact">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-lg-3">
                        <div class="agent1">
                            <a href="#" class="agent-profile-img">
                                <img src="{{url('img/afam.jpg')}}" alt="Chioma Okafor">
                                <div class="ring-effect"></div>
                            </a>
                            <h3 class="agent-name">AFAM NRI CHARLES</h3>
                            <div>
                                <ul>
                                    <li><span class="fa fa-envelope" aria-hidden="true"></span> <p>afam@afamnriconsulting.com.ng</p></li>
                                    <li><span class="fa fa-phone-square" aria-hidden="true"></span>08035884423</li>

                                </ul>
                            </div>
                            <div class="agent-contact">
                            </div>
                        </div>
                    </div>


                    {{--@foreach($users as $item)--}}
                    {{--<div class="col-xs-12 col-sm-6 col-lg-3">--}}
                        {{--<div class="agent1">--}}
                            {{--<a href="{{url('agent/' . $item->uid)}}" class="agent-profile-img">--}}
                                {{--<img src="{{$item->image}}" alt="{{$item->name}}">--}}
                                {{--<div class="ring-effect"></div>--}}
                            {{--</a>--}}
                            {{--<h3 class="agent-name">{{$item->name}}</h3>--}}
                            {{--<div>--}}
                                {{--<ul>--}}
                                    {{--<li><span class="fa fa-envelope" aria-hidden="true"></span> <p>{{$item->email}}</p></li>--}}
                                    {{--<li><span class="fa fa-phone-square" aria-hidden="true"></span> {{$item->phone}}</li>--}}

                                {{--</ul>--}}
                            {{--</div>--}}
                            {{--<div class="agent-contact">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--@endforeach--}}
                </div>
            </div>
            <div class="divider-medium"></div>
        </div>
    </section>
    <!-- End Agent Section -->



    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<div class="col-md-8 col-md-offset-2">--}}
                {{--@include('notification')--}}
                {{--<div class="panel panel-default">--}}
                    {{--<div class="panel-heading">Dashboard</div>--}}

                    {{--<div class="panel-body">--}}

                        {{--<table class="table table-responsive">--}}
                            {{--<tr>--}}
                                {{--<th>Title</th>--}}
                                {{--<th>Purpose</th>--}}
                                {{--<th>Price</th>--}}
                                {{--<th>City</th>--}}
                                {{--<th></th>--}}
                            {{--</tr>--}}

                            {{--@foreach($properties as $item)--}}

                                {{--<tr>--}}
                                    {{--<td>{{$item->title}}</td>--}}
                                    {{--<td>{{$item->purpose}}</td>--}}
                                    {{--<td>{{$item->price}}</td>--}}
                                    {{--<td>{{$item->city}}</td>--}}
                                    {{--<td>--}}
                                        {{--<a href="{{url('/property/' . $item->pid)}}" class="btn btn-primary">View</a>--}}
                                    {{--</td>--}}
                                {{--</tr>--}}

                            {{--@endforeach--}}
                        {{--</table>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
@endsection
