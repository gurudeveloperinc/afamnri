@extends('layouts.app')

@section('content')




    <!-- //Stats -->
    <div class="agitsworkw3ls" id="property">

        <div class="col-md-6 agitsworkw3ls-grid ">
            <h3 class="tittle two">AFAM NRI CONSULTING </h3>
            <!-- Tabs -->
            <div class="tabs">
                <div class="sap_tabs">
                    <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                        <ul class="resp-tabs-list">
                            <li class="resp-tab-item resp-tab-active" aria-controls="tab_item-0" role="tab"><span style="color:black">ABOUT</span></li>
                            <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span style="color:black">MISSION</span></li>
                            <li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span style="color:black">CORPORATION</span></li>
                            <div class="clearfix"></div>
                        </ul>

                        <div class="resp-tabs-container">
                            <h3 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>CREATION</h3><div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
                                <p>AFAM NRI CONSULTING is a firm of Chartered Real Estate Surveyors and Valuers with bias in Property Valuation. The firm also undertakes Feasibility and Viability Appraisals/Development Consultancy, Facility Management, Real Estate Agency and Real Estate Management.</p>
                                <p>We are therefore affiliated with the Nigerian Institution of Estate Surveyors and Valuers (NIESV), and are duly registered with the Estate Surveyors and Valuers Registration Board of Nigeria (ESVRBON).</p>
                            </div>
                            <h3 class="resp-accordion" role="tab" aria-controls="tab_item-1"><span class="resp-arrow"></span>ANALYSIS</h3><div class="tab-2 resp-tab-content" aria-labelledby="tab_item-1">
                                <p>To assist clients realize their dreams on real and personal properties through sound professional practice in all aspects of estate surveying and valuation” with “Integrity, Team Work, Excellence, Care and Passion for clients” being our core values (ITECP)..</p>
                                <p>We offer a range of professional services which we believe will be of benefit to your organization. </p><br><br><br>

                            </div>
                            <h3 class="resp-accordion" role="tab" aria-controls="tab_item-2"><span class="resp-arrow"></span>STRATEGY</h3><div class="tab-3 resp-tab-content" aria-labelledby="tab_item-2">
                                <p> For service excellence, enhanced cohesion and better portfolio management, we are organized along Real Estate Surveying and Valuation for all purposes; Property/Investment Development, Facilities/Property Management and Real Estate Agency Departments.

                                    Our corporate office is located at:
                                    Suite C12, Second Floor, Al-Maliha Plaza,
                                    Opposite KIA Motors, Area 11, Garki, Abuja
                                    Telc, 08187191304, 08092708822

                                    .</p>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Tabs -->

        </div>

        <div class="col-md-6 agitsworkw3ls-grid-2">

            <div class="resp-tabs-container">
                <h3 class="resp-accordion resp-tab-active" role="tab" aria-controls="tab_item-0"><span class="resp-arrow"></span>CREATION</h3><div class="tab-1 resp-tab-content resp-tab-content-active" aria-labelledby="tab_item-0" style="display:block">
                    <p><b>WHY YOU NEED TO VALUE YOUR ASSETS</b>.</p>
                    <p>Corporate bodies and individuals require valuations of various kinds of facilities due to reasons such as obtaining or giving out a loan facility, insurance cover, mergers and acquisitions, amongst others. We ensure that properties and assets are rightly valued to make brilliant investment decisions..</p>

                    <p><b>Insurance.</b></p>
                    <p>Prior to the consumption of an insurance contract, a figure has to be put forward as a sum insured and this will form the basis of calculation of the premium to be paid and the limit of claims. </p>

                    <p><b>External Financing/Mortgages</b></p>
                    <p>When a loan is contemplated or is in the process of being granted, the lending institution will ask for collateral for the loan being sought. This is where we come in to determine the value of the borrower’s collateral, which will act as an appropriate guide to the lending institution.

                    </p>
                    <p><b>Balance Sheet/Annual Accounts</b></p>
                    <p>More often than not, most organization do not know the exact worth of their assets. All they rely on is “historic” cost of acquisition of the assets depreciated along the line over the years in spite of the fact that in the mind of the Boards of Directors and Management they are fully aware that the value is substantially higher or lower than what it is on their books.</p>

                </div>






            </div>
        </div>
    </div>

    </div>
    <div class="clearfix"></div>

    </div>
    <!-- //Stats -->
    <!--/team-->
    <div class="team" id="team">
        <div class="container">
            <h3 class="tittle">Partners</span></h3>

            <!--/about-section-->
            <div class="about-section agileits">
                <div class="col-md-7 ab-left">
                    <div class="grid" align="center">
                        <div class="h-f">
                            <figure class="effect-jazz">
                                <img src="{{url('img/afam.jpg')}}" alt="img25">
                                <figcaption>
                                    {{--<h4>Afam charles nri</h4>--}}
                                    {{--<ul class="top-links two">--}}
                                        {{--<li><a href="#"><i class="fa fa-facebook"></i></a></li>--}}
                                        {{--<li><a href="#"><i class="fa fa-twitter"></i></a></li>--}}
                                        {{--<li><a href="#"><i class="fa fa-linkedin"></i></a></li>--}}
                                        {{--<li><a href="#"><i class="fa fa-google-plus"></i></a></li>--}}
                                    {{--</ul>--}}
                                    <p>
                                        Principle Partner
                                    </p>

                                </figcaption>
                            </figure>

                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-5 ab-text">
                    <h4 class="tittle">Afam charles Nri</h4>
                    <p><b>Principle Partner</b> He started his career with Zenith Bank Plc where he worked in Risk Management department before proceeding to Clem Eze & Co where he rose to the exalted position of General Manager of the firm before establishing the firm of AFAM NRI CONSULTING.</p></div>

                <div class="clearfix"> </div>
            </div>
            <!--//about-section-->

        </div>
    </div>
    <!--/team-->

@endsection