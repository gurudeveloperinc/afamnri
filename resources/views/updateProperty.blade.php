@extends('layouts.app')

@section('content')

    <div class="container">

        <br><br>

        <div class="panel panel-default col-md-8 col-md-offset-2">

            @include('notification')

            <div class="panel-heading">Update a property</div>
            <div class="panel-body">
                <form method="post" class="addProperty" enctype="multipart/form-data" action="{{url('/property/' . $property->pid . '/update')}}">

                    {{csrf_field()}}
                    <input multiple onchange="readURL(this);" type="file" id="hiddenFile1" class="hidden" name="images[]">

                    <input type="hidden" name="pid" value="{{$property->pid}}">
                    <div class="form-group">

                        <label for="title" class="col-md-4 control-label">Title</label>

                        <div class="col-md-8">
                            <input id="title" type="text" class="form-control" name="title" required value="{{ $property->title  }}" >
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="description" class="col-md-4 control-label">Description</label>

                        <div class="col-md-8">
                            <textarea class="form-control" name="description" required>{{ $property->description}}</textarea>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="price" class="col-md-4 control-label">Price</label>

                        <div class="col-md-8">
                            <input id="price" type="text" placeholder="Eg. 40000" pattern="[0-9]+" class="form-control" name="price" required value="{{ $property->price }}" maxlength="11" >
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="purpose" class="col-md-4 control-label">Purpose</label>

                        <div class="col-md-8">
                            <select class="form-control" required name="purpose">
                                <option
                                        @if($property->purpose == "Rent")
                                        selected
                                        @endif

                                        value="Rent">For Rent</option>
                                <option value="Sale"
                                    @if($property->purpose == "Rent")
                                    selected
                                    @endif

                                >For Sale</option>
                            </select>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>

                    </div>

                    <div class="form-group">
                        <label for="type" class="col-md-4 control-label">Type</label>

                        <div class="col-md-8">
                            <select class="form-control" required name="type">
                                <option
                                    @if($property->type == "Residential")
                                        selected
                                    @endif
                                  >Residential</option>
                                <option
                                    @if($property->type == "Office Space")
                                    selected
                                    @endif

                                >Office Space</option>
                            </select>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="lga" class="col-md-4 control-label">LGA</label>

                        <div class="col-md-8">
                            <input id="lga" type="text" class="form-control" name="lga" value="{{ $property->lga }}" >
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="city" class="col-md-4 control-label">City</label>

                        <div class="col-md-8">
                            <input id="city" type="text" required class="form-control" name="city" value="{{ $property->city }}" >
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>


                    <div class="form-group">

                        <label for="type" class="col-md-4 control-label">State</label>

                        <div class="col-md-8">
                            <select class="form-control" required name="state">

                                <option @if($property->state == "Abuja") selected @endif>Abuja</option>
                                <option @if($property->state == "Abia") selected @endif>Abia</option>
                                <option @if($property->state == "Adamawa") selected @endif>Adamawa</option>
                                <option @if($property->state == "Anambra") selected @endif>Anambra</option>
                                <option @if($property->state == "Akwa Ibom") selected @endif>Akwa Ibom</option>
                                <option @if($property->state == "Bauchi") selected @endif>Bauchi</option>
                                <option @if($property->state == "Bayelsa") selected @endif>Bayelsa</option>
                                <option @if($property->state == "Benue") selected @endif>Benue</option>
                                <option @if($property->state == "Borno") selected @endif>Borno</option>
                                <option @if($property->state == "Cross River") selected @endif>Cross River</option>
                                <option @if($property->state == "Delta") selected @endif>Delta</option>
                                <option @if($property->state == "Ebonyi") selected @endif>Ebonyi</option>
                                <option @if($property->state == "Enugu") selected @endif>Enugu</option>
                                <option @if($property->state == "Edo") selected @endif>Edo</option>
                                <option @if($property->state == "Ekiti") selected @endif>Ekiti</option>
                                <option @if($property->state == "Gombe") selected @endif>Gombe</option>
                                <option @if($property->state == "Imo") selected @endif>Imo</option>
                                <option @if($property->state == "Jigawa") selected @endif>Jigawa</option>
                                <option @if($property->state == "Kaduna") selected @endif>Kaduna</option>
                                <option @if($property->state == "Kano") selected @endif>Kano</option>
                                <option @if($property->state == "Katsina") selected @endif>Katsina</option>
                                <option @if($property->state == "Kebbi") selected @endif>Kebbi</option>
                                <option @if($property->state == "Kogi") selected @endif>Kogi</option>
                                <option @if($property->state == "Kwara") selected @endif>Kwara</option>
                                <option @if($property->state == "Lagos") selected @endif>Lagos</option>
                                <option @if($property->state == "Nasarawa") selected @endif>Nasarawa</option>
                                <option @if($property->state == "Niger") selected @endif>Niger</option>
                                <option @if($property->state == "Ogun") selected @endif>Ogun</option>
                                <option @if($property->state == "Ondo") selected @endif>Ondo</option>
                                <option @if($property->state == "Osun") selected @endif>Osun</option>
                                <option @if($property->state == "Oyo") selected @endif>Oyo</option>
                                <option @if($property->state == "Plateau") selected @endif>Plateau</option>
                                <option @if($property->state == "Rivers") selected @endif>Rivers</option>
                                <option @if($property->state == "Sokoto") selected @endif>Sokoto</option>
                                <option @if($property->state == "Taraba") selected @endif>Taraba</option>
                                <option @if($property->state == "Yobe") selected @endif>Yobe</option>
                                <option @if($property->state == "Zamfara") selected @endif>Zamfara</option>
                            </select>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="address" class="col-md-4 control-label">Address</label>

                        <div class="col-md-8">
                            <textarea class="form-control" required name="address">{{ $property->address }}</textarea>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>
                    </div>



                    <div class="form-group">
                        <label for="map" class="col-md-4 control-label">Map Url</label>

                        <div class="col-md-8">
                            <input id="map" type="text" class="form-control" name="map" value="{{ $property->map }}" >
                        </div>
                    </div>

                    <div class="form-group">

                        <label for="agent" class="col-md-4 control-label">Agent</label>

                        <div class="col-md-8">
                            <select class="form-control" required name="uid">

                                @foreach($staff as $item)
                                    <option
                                            @if($item->uid == $property->uid)
                                            selected
                                            @endif
                                            value="{{$item->uid}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>

                    </div>



                    <div class="row">
                        <div id="preview" class="postImagePreview">

                            @foreach($property->Images as $item)
                            <div data-imid="{{$item->imid}}" class="col-md-6 image" ondblclick="removeItem(this)" onmouseleave="removeHover(this)"  onmouseover="hover(this)">
                                <img  src ="{{$item->url}}">
                            </div>
                            @endforeach
                        </div>
                    </div>


                    <div class="form-group col-md-12">
                        <div class="col-md-6 col-md-offset-4">
                            <a  id="addImageButton" class="btn btn-primary">Add Images</a>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>




    <script>

        $(document).ready(function () {

            $('#addImageButton').on('click',function(){
                $('#hiddenFile1').click();
            });


        });
        function hover(item){

            $(item).css('background-color','red');
            $(item).children('img').css('opacity',0.5);


        }

        function removeHover(item){
            $(item).css('background-color','transparent');
            $(item).children('img').css('opacity',1);
        }

        function removeItem(item){

             var imid = $(item).data('imid');
            var url = '{{url('delete-image')}}/' + imid;
            console.log(url);
            $.get(url,function(){
                console.log('success');
            },function(){
                $(item).remove();
                console.log('An error occurred');
            });

            $(item).remove();

        }

        function readURL(input) {

            for(i=0; i< input.files.length; i++) {


                if (input.files && input.files[i]) {


                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#preview').append('<div class="col-md-6 image" onclick="removeItem(this)" onmouseleave="removeHover(this)"  onmouseover="hover(this)"> <img src ="' + e.target.result + '"></div>');
                    };


                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

    </script>
@endsection