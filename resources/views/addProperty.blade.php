@extends('layouts.app')

@section('content')

<div class="container">

    <br><br>

<div class="panel panel-default col-md-8 col-md-offset-2">

    @include('notification')

    <div class="panel-heading">Add a property</div>
    <div class="panel-body">
        <form method="post" class="addProperty" enctype="multipart/form-data" action="{{url('/add-property')}}">

            {{csrf_field()}}
            <input multiple onchange="readURL(this);" type="file" id="hiddenFile1" class="hidden" name="images[]">

                     <div class="form-group">

                         <label for="title" class="col-md-4 control-label">Title</label>

                         <div class="col-md-8">
                             <input id="title" type="text" class="form-control" name="title" required value="{{ old('title') }}" >
                             <span style="color:red; font-size: 12px;float: left">*required</span><br>
                         </div>
                     </div>


                     <div class="form-group">
                         <label for="description" class="col-md-4 control-label">Description</label>

                         <div class="col-md-8">
                             <textarea class="form-control" name="description" required>{{old('description')}}</textarea>
                             <span style="color:red; font-size: 12px;float: left">*required</span><br>
                         </div>
                     </div>



                     <div class="form-group">
                         <label for="price" class="col-md-4 control-label">Price</label>

                         <div class="col-md-8">
                             <input id="price" type="text" placeholder="Eg. 40000" pattern="[0-9]+" class="form-control" name="price" required value="{{ old('price') }}" maxlength="11" >
                             <span style="color:red; font-size: 12px;float: left">*required</span><br>
                         </div>
                     </div>

                    <div class="form-group">

                        <label for="purpose" class="col-md-4 control-label">Purpose</label>

                        <div class="col-md-8">
                            <select class="form-control" required name="purpose">
                                <option selected value="Rent">For Rent</option>
                                <option value="Sale">For Sale</option>
                            </select>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>

                    </div>
                     
                     <div class="form-group">
                         <label for="type" class="col-md-4 control-label">Type</label>
                         
                         <div class="col-md-8">
                             <select class="form-control" required name="type">
                                 <option selected >Residential</option>
                                 <option>Office Space</option>
                             </select>
                             <span style="color:red; font-size: 12px;float: left">*required</span><br>
                         </div>
                     </div>
                     
                     
                     <div class="form-group">
                         <label for="lga" class="col-md-4 control-label">LGA</label>
                         
                         <div class="col-md-8">
                             <input id="lga" type="text" class="form-control" name="lga" value="{{ old('lga') }}" >
                         </div>
                     </div>
                     
                     
                     <div class="form-group">
                         <label for="city" class="col-md-4 control-label">City</label>
                         
                         <div class="col-md-8">
                             <input id="city" type="text" required class="form-control" name="city" value="{{ old('city') }}" >
                             <span style="color:red; font-size: 12px;float: left">*required</span><br>
                         </div>
                     </div>

            <div class="form-group">

                <label for="type" class="col-md-4 control-label">State</label>

                <div class="col-md-8">
                    <select class="form-control" required name="state">

                        <option >Abuja</option>
                        <option >Abia</option>
                        <option >Adamawa</option>
                        <option >Anambra</option>
                        <option >Akwa Ibom</option>
                        <option >Bauchi</option>
                        <option >Bayelsa</option>
                        <option >Benue</option>
                        <option >Borno</option>
                        <option >Cross River</option>
                        <option >Delta</option>
                        <option >Ebonyi</option>
                        <option >Enugu</option>
                        <option >Edo</option>
                        <option >Ekiti</option>
                        <option >Gombe</option>
                        <option >Imo</option>
                        <option >Jigawa</option>
                        <option >Kaduna</option>
                        <option >Kano</option>
                        <option >Katsina</option>
                        <option >Kebbi</option>
                        <option >Kogi</option>
                        <option >Kwara</option>
                        <option >Lagos</option>
                        <option >Nasarawa</option>
                        <option >Niger</option>
                        <option >Ogun</option>
                        <option >Ondo</option>
                        <option >Osun</option>
                        <option >Oyo</option>
                        <option >Plateau</option>
                        <option >Rivers</option>
                        <option >Sokoto</option>
                        <option >Taraba</option>
                        <option >Yobe</option>
                        <option >Zamfara</option>
                    </select>
                    <span style="color:red; font-size: 12px;float: left">*required</span><br>
                </div>
            </div>



            <div class="form-group">
                         <label for="address" class="col-md-4 control-label">Address</label>
                         
                         <div class="col-md-8">
                            <textarea class="form-control" required name="address">{{ old('address') }}</textarea>
                             <span style="color:red; font-size: 12px;float: left">*required</span><br>
                         </div>
                     </div>
            
            
                     
                     <div class="form-group">
                         <label for="map" class="col-md-4 control-label">Map Url</label>
                         
                         <div class="col-md-8">
                             <input id="map" type="text" class="form-control" name="map" value="{{ old('map') }}" >
                         </div>
                     </div>

                    <div class="form-group">

                        <label for="agent" class="col-md-4 control-label">Agent</label>

                        <div class="col-md-8">
                            <select class="form-control" required name="uid">

                                @foreach($staff as $item)
                                <option selected value="{{$item->uid}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            <span style="color:red; font-size: 12px;float: left">*required</span><br>
                        </div>

                    </div>



            <div class="row">
                <div id="preview" class="postImagePreview"></div>
            </div>


            <div class="form-group col-md-12">
                        <div class="col-md-6 col-md-offset-4">
                            <a  id="addImageButton" class="btn btn-primary">Add Images</a>
                            <button type="submit" class="btn btn-success">Save</button>
                        </div>
                    </div>


        </form>
    </div>
</div>
</div>




<script>

    $(document).ready(function () {

        $('#addImageButton').on('click',function(){
            $('#hiddenFile1').click();
        });


    });
    function hover(item){

        console.log('hovered');
        $(item).css('background-color','red');
        $(item).children('img').css('opacity',0.5);
        console.log($(item).children('img'));


    }

    function removeHover(item){
        console.log('cleared');
        $(item).css('background-color','transparent');
        $(item).children('img').css('opacity',1);
    }

    function removeItem(item){
        console.log('removed');
        $(item).remove();
    }

    function readURL(input) {

        for(i=0; i< input.files.length; i++) {


            if (input.files && input.files[i]) {


                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#preview').append('<div class="col-md-6 image" onclick="removeItem(this)" onmouseleave="removeHover(this)"  onmouseover="hover(this)"> <img src ="' + e.target.result + '"></div>');
                };


                reader.readAsDataURL(input.files[i]);
            }
        }
    }

</script>
@endsection