
<!DOCTYPE html>
<html lang="en">
<head>
    <title>404 - Afam Nri Consulting</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- font files -->
    <link href="//fonts.googleapis.com/css?family=Wallpoet" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Hind" rel="stylesheet">
    <!-- /font files -->
    <!-- css files -->
    <link href="{{url('error/error.css')}}" rel="stylesheet" type="text/css" media="all" />
    <!-- /css files -->
<body>
<div class="w3layouts-bg">

    <div class="agileits-content">
        <h2><span>4</span><span>0</span><span>4</span></h2>

    </div>
    <div class="w3layouts-right">
        <div class="w3ls-text">
            <a href="{{url('/')}}" style="position: absolute;right: 50px;">
                <img src="{{url('images2/afam.jpg')}}" alt="logo">
            </a>

            <h3>We're sorry!</h3>
            <h4 class="w3-agileits2">The page you requested could not be found.</h4>

            <p>Please ensure you typed the right link <br>
                <a href="{{url('/')}}" style="font-size: 20px; background-color: #FFA500; color:white;padding: 5px;border-radius: 5px;">Go Back</a>
                <br>
                <a href="mailto:info@afamnriconsulting.com.ng">info@afamnriconsulting.com.ng</a>
            </p>

            <p class="copyright">© <?php echo date('Y'); ?> Afam Nri Consulting. All Rights Reserved <br> Developed by <a href="https://gurudeveloperinc.com/" target="_blank" style="color:#FFA500">GuruDeveloper Inc.</a></p>

        </div>

    </div>
    <div class="clearfix"></div>
</div>

</body>
</html>