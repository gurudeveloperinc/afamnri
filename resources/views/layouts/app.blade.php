<!DOCTYPE html>
<html>
<head>
    <title>Afam Nri Consulting | Home </title>
    <!-- for-mobile-apps -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Properties for rent in Abuja,Properties for rent in Abuja, Properties for rent in nigeria, Rent, Sale, Properties for Sale, Properties in Nigeria " />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- //for-mobile-apps -->
    <link href="{{url('css2/bootstrap.css')}}" rel="stylesheet" type="text/css" media="all" />
    <link rel="stylesheet" type="text/css" href="{{url('css2/zoomslider.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('css2/style.css')}}" />

    <link rel="stylesheet" href="{{url('css/owl.carousel.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('css/owl.theme.css')}}" type="text/css">


    <link href="{{url('css2/font-awesome.min.css')}}" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="{{url('js2/modernizr-2.6.2.min.js')}}"></script>
    <link rel="stylesheet" href="{{url('css/elegant_font/style.css')}}" type="text/css">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{url('favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('favicon/favicon-16x16.png')}}">
    <!-- Template Stylesheet -->
    <link rel="stylesheet" href="{{url('css/style.css')}}" type="text/css">
    <!-- Responsive Stylesheet -->
    <link rel="stylesheet" href="{{url('css/responsive.css')}}" type="text/css">
    <link rel="stylesheet" href="{{url('css/custom.css')}}" type="text/css">

    <link href='//fonts.googleapis.com/css?family=Tangerine:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Lato:400,300,300italic,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

    <link href="{{url('css/photoswipe.css')}}" rel="stylesheet">
    <link href="{{url('css/default-skin/default-skin.css')}}" rel="stylesheet">

    <script>
        window.Laravel = <?php echo json_encode([
			'csrfToken' => csrf_token(),
		]); ?>

        const baseUrl = '<?php echo url('/'); ?>'
    </script>

    <script src="{{url('js/jquery.js')}}"></script>
    <script src="{{url('js/photoswipe.min.js')}}"></script>
    <script src="{{url('js/photoswipe-ui-default.min.js')}}"></script>

    <!--//web-fonts-->
</head>
<body>
<!--/main-header-->
<div class="top-main" id="home">
    <div class="header-top">
        <div class="w3l_header_left">
            <ul>
                <li><span class="fa fa-envelope" aria-hidden="true"></span> <a href="info@afamnriconsulting.com.ng ">info@afamnriconsulting.com.ng </a></li>
                <li><span class="fa fa-phone-square" aria-hidden="true"></span>Abuja| 08187191304</li>
                <li><span class="fa fa-phone-square" aria-hidden="true"></span>PortHarcourt| 08092728822</li>
                <li><span class="fa fa-phone-square" aria-hidden="true"></span>Anambra| 09099599746</li>
                <li style="float:right;"><a href="http://afamnriconsulting.com.ng/webmail" >Mail</a></li>

                @if(Auth::guest())
                    <li style="float:right;"><a href="{{url('login')}}" >Login</a></li>

                @else
                    <li style="float:right;"> Logged in as {{Auth::user()->name}}
                        &nbsp;&nbsp; <a style="color:white;" class="btn btn-primary" href="{{url('dashboard')}}" >Dashboard</a>
                        &nbsp;&nbsp; <a style="color:white;" class="btn btn-primary" href="{{url('logout')}}" >Logout</a>
                    </li>
                @endif
            </ul>
        </div>

        <div class="clearfix"> </div>
    </div>
    <div class="header-bottom">
        <div class="container">
            <nav class="navbar navbar-default">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                        <a href="{{url('/')}}">
                            <img src="{{url('images2/afam.jpg')}}" alt="Afam Nri Consulting Logo">
                        </a>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                    <nav class="cl-effect-1" id="cl-effect-1">
                        <ul class="nav navbar-nav">
                            <li><a href="{{url('/')}}">Home</a></li>
                            <li><a href="{{url('about')}}" >About Us</a></li>
                            <li><a href="{{url('services')}}" >Services</a></li>
                            <li><a href="{{url('property-search')}}" >Property Search</a></li>
                            <li><a href="{{url('team')}}" >Team</a></li>
                            <li><a href="{{url('contact')}}" >Contact</a></li>

                    </nav>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
            <div class="w3ls_search">
                <div class="cd-main-header">
                    <ul class="cd-header-buttons">
                        <li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
                    </ul> <!-- cd-header-buttons -->
                </div>
                <div id="cd-search" class="cd-search">
                    <form action="#" method="post">
                        <input name="Search" type="search" placeholder="Search...">
                    </form>
                </div>
            </div>

        </div>
    </div>


        @yield('content')




    <div class="contact-w3ls" id="contact">
        <div class="container">


            <div class="footer-grids w3-agileits">
                <div align="center" class="col-md-4 footer-grid">
                    <p align="center">
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                        Abuja | 08187191304, 08092708822<br>
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                        Anambra | 08035884423, 09099599746<br>
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                        PortHarcourt | 08092728822 , 09098629816 <br>
                        <span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>
                        Lagos | 07062512262 <br>
                    </p>
                </div>
                <div align="center" class="col-md-4 footer-grid">
                    <ul>

                        {{--<li class="home.html"><a class="scroll" href="index.html">Home</a></li>--}}
                        {{--<li><a href="About.html" class="scroll hvr-bounce-to-bottom">About Us</a></li>--}}
                        {{--<li><a href="services.html" class="scroll hvr-bounce-to-bottom">Services</a></li>--}}
                        {{--<li><a href="property.html" class="scroll hvr-bounce-to-bottom">Property Search</a></li>--}}
                        {{--<li><a href="team.html" class="scroll hvr-bounce-to-bottom">Team</a></li>--}}
                        {{--<li><a href="contact.html" class="scroll hvr-bounce-to-bottom">Contact</a></li>--}}
                    </ul>
                </div>
                <div class="col-md-4 footer-grid">
                    <div align="center" class="footer-grid1">
                        <div>
                            <a href="{{url('/')}}" style="width: 200px"><img style="width: 100px" src="{{url('images2/afam.jpg')}}" alt=" " class="img-responsive"></a> <br>
                            <a href="{{url('/')}}" style="color: #fff;">AFAM NRI CONSULTING </a>
                        </div>
                        <div class="divider-small"></div>
                        <p style="text-align: center;color:white;">
                            Suite C12, Second Floor, Al-Maliha Plaza,
                            Opposite KIA Motors, Area 11, Garki, Abuja.
                        </p>
                        <p>
                            <a style="text-align: center;" href="mailto:contact@afamnriconsulting.com.ng"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>: info@afamnriconsulting.com.ng</a>
                        </p>


                        <br>
                    </div>

                </div>

                <div class="divider-small"></div>
            </div>

            <div class="w3agile_footer_copy">
                <a href="#" class="btn btn-sm button-icon btn-round btn-link"><span
                            class="fa fa-link"></span></a>
                <a href="#" class="btn btn-sm button-icon btn-round btn-facebook"><span
                            class="fa fa-facebook"></span></a>
                <a href="#" class="btn btn-sm button-icon btn-round btn-twitter"><span
                            class="fa fa-twitter"></span></a>
                <a href="#" class="btn btn-sm button-icon btn-round btn-google"><span
                            class="fa fa-google-plus"></span></a>
                <p>© <?php echo date('Y'); ?>. All rights reserved | Developed by <a href="http://www.gurudeveloperinc.com">GuruDeveloper Inc.</a></p>
            </div>
        </div>

    </div>

    <a href="#home" id="toTop" class="scroll" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/596ca16c6edc1c10b0346570/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    {{--<script src="js2/jquery-1.11.1.min.js"></script>--}}
    <script type="text/javascript" src="{{url('js2/jquery.zoomslider.min.js')}}"></script>
    <!-- search-jQuery -->
    <script src="{{url('js2/main.js')}}"></script>
    <!-- //search-jQuery -->
    <script type="text/javascript">
        $(window).load(function() {
            $("#flexiselDemo1").flexisel({
                visibleItems:3,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 3000,
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint:480,
                        visibleItems: 1
                    },
                    landscape: {
                        changePoint:640,
                        visibleItems: 2
                    },
                    tablet: {
                        changePoint:768,
                        visibleItems: 2
                    }
                }
            });

        });
    </script>
    <script type="text/javascript" src="{{url('js2/jquery.flexisel.js')}}"></script>

    <!-- Horizontal-Tabs-JavaScript -->
    <script src="{{url('js2/easyResponsiveTabs.js')}}" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#horizontalTab').easyResponsiveTabs({
                type: 'default',
                width: 'auto',
                fit: true,
            });
        });
    </script>
    <!--start-smooth-scrolling-->
    <!--/script-->
    <script type="text/javascript" src="{{url('js2/move-top.js')}}"></script>
    <script type="text/javascript" src="{{url('js2/easing.js')}}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},900);
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */

            $().UItoTop({ easingType: 'easeOutQuart' });

        });
    </script>

    <!-- Fancybox (Popup) JavaScript -->
    <script src="{{url('js/jquery.fancybox.pack.js')}}"></script>
    <!-- OwlCarousel (Slider) JavaScript -->
    <script src="{{url('js/owl.carousel.js')}}"></script>

    <!-- Fancybox Popup jQuery -->
    <script src="{{url('js/jquery.fancybox.pack.js')}}"></script>
    <!-- Bootstrap JavaScript -->
    <script src="{{url('js/jquery.validate.js')}}"></script>

    <!--end-smooth-scrolling-->
    <script src="{{url('js2/bootstrap.min.js')}}"></script>
    <script src="{{url('js/common.js')}}"></script>
</body>
</html>