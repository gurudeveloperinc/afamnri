@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1 dashboard">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <a href="{{url('/dashboard')}}" class="btn btn-primary" style="margin: 5px">Go back</a>
                    <div align="center" class="panel-body">

                        <div class="row">
                            <br><br>
                            <h3 align="center">All Properties</h3>
                            <table class="table table-responsive">
                                <tr>
                                    <th>Title</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Purpose</th>
                                    <th>Price</th>
                                    <th>Agent</th>
                                    <th></th>
                                    <th></th>
                                </tr>


                                @foreach($propertiesByYou as $property)

                                        <tr>
                                            <td>{{$property->title}}</td>
                                            <td>{{$property->city}}</td>
                                            <td>{{$property->state}}</td>
                                            <td>{{$property->purpose}}</td>
                                            <td>{{number_format($property->price)}}</td>
                                            <td>{{$property->Agent->name}}</td>
                                            <td>
                                                <a href="{{url('/property/' . $property->pid)}}">View</a>
                                            </td>
                                            <td>
                                                <a href="{{url('/property/' . $property->pid . '/update')}}">Update</a>
                                            </td>

                                        </tr>

                                @endforeach
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
