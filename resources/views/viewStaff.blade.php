@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('notification')
            <div class="col-md-8 col-md-offset-2 dashboard">
                <div class="panel panel-default">
                    <div class="panel-heading">Dashboard</div>

                    <a href="{{url('/dashboard')}}" class="btn btn-primary" style="margin: 5px">Go back</a>
                    <div align="center" class="panel-body">

                        <div class="row">
                            <br><br>
                            <h3 align="center">All Staff</h3>
                            <table class="table table-responsive">
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th></th>
                                </tr>


                                @foreach($staff as $item)

                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->email}}</td>
                                        <td>{{$item->phone}}</td>
                                        <td>
                                            <a href="{{url('/delete-staff/' . $item->uid)}}">Delete</a>
                                        </td>
                                    </tr>

                                @endforeach
                            </table>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
